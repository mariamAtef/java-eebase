package com.sumerge.program;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.net.URI;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("department")
public class DepartmentResources {
    private static final Logger LOGGER = Logger.getLogger(DepartmentResources.class.getName());

    @EJB
    private DepartmentRepo departmentRepo ;


    @Context
    HttpServletRequest request;

    @Context
    private SecurityContext securityContext;



    @GET
    public Response getAllDepartments() {
        LOGGER.info("Entering get with department " + securityContext.getUserPrincipal().toString());
        try {
            return Response.ok().entity(departmentRepo.getAllDepartment()).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }


    @GET
    @Path("{id}")
    public Response getDepartmentById(@PathParam("id") String id) {
        LOGGER.info("Entering get with department " + securityContext.getUserPrincipal().toString());
        try {
            return Response.ok().entity(departmentRepo.getCertainDepartment(id)).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }



    @DELETE
    @Path("{id}")
    public Response deleteDepartment(@PathParam("id") String id) {
        LOGGER.info("Entering delete with department " + securityContext.getUserPrincipal().toString());
        try {
            departmentRepo.deleteDepartment(id);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }


    @POST
    public Response addDepartment(Department department) {
        LOGGER.info("Entering post with department " + securityContext.getUserPrincipal().toString());
        try {
            if (department == null) {
                throw new IllegalArgumentException("Can't create student since it exists in the database");
            }
            departmentRepo.addDepartment(department);
            URI uri = new URI(request.getRequestURI());
            return Response.created(uri).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }

    @PUT
    public Response editDepartment(Department department) {
        LOGGER.info("Entering put with department " + securityContext.getUserPrincipal().toString());
        try {
            if (department == null) {
                throw new IllegalArgumentException(
                        "Can't edit department since it does not exist in the database");
            }
            departmentRepo.updateDepartment(department);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }
}
