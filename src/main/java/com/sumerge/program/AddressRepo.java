package com.sumerge.program;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class AddressRepo {

    @PersistenceContext(unitName = "MyPU")
    private EntityManager em;


    public List<Address> getAllAddresses() {
        try {
            String sql = "SELECT a FROM Address a";
            List<Address> addressDetails = em.createQuery(sql, Address.class).getResultList();
            return addressDetails;
        } catch (JpaException ex) {
            throw new JpaException(ex.getMessage(), ex);
        }
    }

    public Address getCertainAddress(String id) {
        try {
            Address addressDetails = em.find(Address.class, id);
            return addressDetails;
        } catch (JpaException ex) {
            throw new JpaException(ex.getMessage(), ex);
        }
    }

    public void addAddress(Address address) {
        try {
            em.getTransaction().begin();
            em.persist(address);
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }

    public void updateAddress(Address address) {
        try {
            em.getTransaction().begin();
            Address details = getCertainAddress(address.getAddressId());
            details.setAddLine1(address.getAddLine1());
            details.setAddLine2(address.getAddLine2());
            details.setCity(address.getCity());
            details.setCountry(address.getCountry());
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }

    public void deleteAddress(String id) {
        try {
            em.getTransaction().begin();
            Address address = getCertainAddress(id);
            em.remove(address);
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }





}
