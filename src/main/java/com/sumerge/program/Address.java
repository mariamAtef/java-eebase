package com.sumerge.program;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "ADDRESS")
public class Address {
    @Id
    @Column(name = "ADDRESSID")
    private String addressId;

    @Transient
    private String newattribute;

    @ManyToOne (fetch = FetchType.EAGER)
    @JoinColumn(name = "EMPID")
    private Employee employeeId;

    @Column(name = "ADDLINE1")
    private String addLine1;

    @Column(name ="ADDLINE2")
    private String addLine2;

    @Column(name ="CITY")
    private String city;

    @Column(name = "REGION")
    private String region;

    @Column(name= "COUNTRY")
    private String country;

    @Column(name = "POSTCODE")
    private String postcode;


    public String getAddressId() {
        return addressId;
    }


    public Employee getEmployeeId() {
        return employeeId;
    }

    public String getAddLine1() {
        return addLine1;
    }

    public String getAddLine2() {
        return addLine2;
    }

    public String getCity() {
        return city;
    }

    public String getRegion() {
        return region;
    }

    public String getCountry() {
        return country;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setAddLine1(String addLine1) {
        this.addLine1 = addLine1;
    }

    public void setAddLine2(String addLine2) {
        this.addLine2 = addLine2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getNewattribute() {
        return this.getPostcode();
    }

    public void setNewattribute(String newattribute) {
        this.newattribute = newattribute;
    }
}
