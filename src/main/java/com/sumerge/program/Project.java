package com.sumerge.program;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "PROJECT")
public class Project {
    @Id
    @Column(name = "PROJID")
    private String projId;

    @Column(name = "PROJNAME")
    private String projName;

    @Column(name = "STARTDATE")
    private Date startDate;

    @Column(name = "TARGETDATE")
    private Date targetDate;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany
    @JoinTable(name = "PROJECTMEMBER", joinColumns = @JoinColumn(name = "PROJID"), inverseJoinColumns = @JoinColumn(name = "EMPID"))
    private List<Employee> empId;



    public String getProjId() {
        return projId;
    }

    public String getProjName() {
        return projName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public void setProjName(String projName) {
        this.projName = projName;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Employee> getEmpId() {
        return empId;
    }

    public void setEmpId(List<Employee> empId) {
        this.empId = empId;
    }
}
