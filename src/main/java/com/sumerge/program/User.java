package com.sumerge.program;

public class User {
  private String name;
  private int id;
  private String email;
  private String addresss;
  private int mobileNumber;

  /**
   * Constructor for the user taking 5 parameters.
   *
   * @param name name of the user.
   * @param id id of the user.
   * @param email email of the user.
   * @param addresss address of the user.
   * @param mobileNumber mobile number of the user.
   */
  public User(String name, int id, String email, String addresss, int mobileNumber) {
    this.name = name;
    this.id = id;
    this.email = email;
    this.addresss = addresss;
    this.mobileNumber = mobileNumber;
  }

  public User() {

  }

  public String getName() {
    return name;
  }

  public int getId() {
    return id;
  }

  public String getEmail() {
    return email;
  }

  public String getAddresss() {
    return addresss;
  }

  public int getMobileNumber() {
    return mobileNumber;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setAddresss(String addresss) {
    this.addresss = addresss;
  }

  public void setMobileNumber(int mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  @Override
  public String toString() {
    return "User{"
        + "name='"
        + name
        + '\''
        + ", id="
        + id
        + ", email='"
        + email
        + '\''
        + ", addresss='"
        + addresss
        + '\''
        + ", mobileNumber="
        + mobileNumber
        + '}';
  }
}
