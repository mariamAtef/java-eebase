package com.sumerge.program;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.net.URI;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("address")
public class AddressResources {
    private static final Logger LOGGER = Logger.getLogger(AddressResources.class.getName());

    @EJB
    private AddressRepo addressRepo ;


    @Context
    HttpServletRequest request;

    @Context
    private SecurityContext securityContext;



    @GET
    public Response getAllAddresses() {
        LOGGER.info("Entering get with address " + securityContext.getUserPrincipal().toString());
        try {
            System.out.println("ADDRESSREPO" + addressRepo) ;
            return Response.ok().entity(addressRepo.getAllAddresses()).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }


    @GET
    @Path("{id}")
    public Response getAddressById(@PathParam("id") String id) {
        LOGGER.info("Entering get with address " + securityContext.getUserPrincipal().toString());
        try {
            return Response.ok().entity(addressRepo.getCertainAddress(id)).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }



    @DELETE
    @Path("{id}")
    public Response deleteAddress(@PathParam("id") String id) {
        LOGGER.info("Entering delete with address " + securityContext.getUserPrincipal().toString());
        try {
            addressRepo.deleteAddress(id);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }


    @POST
    public Response addAddress(Address address) {
        LOGGER.info("Entering post with address " + securityContext.getUserPrincipal().toString());
        try {
            if (address == null) {
                throw new IllegalArgumentException("Can't create student since it exists in the database");
            }
            addressRepo.addAddress(address);
            URI uri = new URI(request.getRequestURI());
            return Response.created(uri).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }

    @PUT
    public Response editAddress(Address address) {
        LOGGER.info("Entering Put with address " + securityContext.getUserPrincipal().toString());
        try {
            if (address == null) {
                throw new IllegalArgumentException(
                        "Can't edit student since it does not exist in the database");
            }
            addressRepo.updateAddress(address);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }
}
