package com.sumerge.program;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "EMPLOYEE")
public class Employee {
    @Id
    @Column(name = "EMPID")
    private String empId;

    @OneToMany
    @JoinColumn(name = "DEPTCODE")
    private List<Department> deptCode;

    @Column(name = "JOBTITLE")
    private String jobtitle;

    @Column(name = "GIVENNAME")
    private String givenName;

    @Column(name = "FAMILYNAME")
    private String familyName;

    @Column(name = "COMMONNAME")
    private String commonName;

    @Column(name = "NAMETITLE")
    private String nameTitle;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "PROJECTMEMBER", joinColumns = @JoinColumn(name = "EMPID"), inverseJoinColumns = @JoinColumn(name = "PROJID"))
    private List<Project> projId;



    public String getEmpId() {
        return empId;
    }


    public String getJobtitle() {
        return jobtitle;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getCommonName() {
        return commonName;
    }

    public String getNameTitle() {
        return nameTitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public void setNameTitle(String nameTitle) {
        this.nameTitle = nameTitle;
    }

    public List<Department> getDeptCode() {
        return deptCode;
    }

    public List<Project> getProjId() {
        return projId;
    }

    public void setDeptCode(List<Department> deptCode) {
        this.deptCode = deptCode;
    }

    public void setProjId(List<Project> projId) {
        this.projId = projId;
    }
}
