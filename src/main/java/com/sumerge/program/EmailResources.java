package com.sumerge.program;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import java.net.URI;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("department")
public class EmailResources {
    private static final Logger LOGGER = Logger.getLogger(EmailResources.class.getName());

    @EJB
    private EmailRepo emailRepo ;


    @Context
    HttpServletRequest request;

    @Context
    private SecurityContext securityContext;



    @GET
    public Response getAllEmails() {
        LOGGER.info("Entering get with email " + securityContext.getUserPrincipal().toString());
        try {
            return Response.ok().entity(emailRepo.getAllEmails()).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }


    @GET
    @Path("{id}")
    public Response getEmailById(@PathParam("id") int id) {
        LOGGER.info("Entering get with email " + securityContext.getUserPrincipal().toString());
        try {
            return Response.ok().entity(emailRepo.getCertainEmail(id)).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }



    @DELETE
    @Path("{id}")
    public Response deleteEmail(@PathParam("id") int id) {
        LOGGER.info("Entering delete with email " + securityContext.getUserPrincipal().toString());
        try {
            emailRepo.deleteEmail(id);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }


    @POST
    public Response addEmail(Email email) {
        LOGGER.info("Entering post with email " + securityContext.getUserPrincipal().toString());
        try {
            if (email == null) {
                throw new IllegalArgumentException("Can't create student since it exists in the database");
            }
            emailRepo.addEmmail(email);
            URI uri = new URI(request.getRequestURI());
            return Response.created(uri).build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }

    @PUT
    public Response editEmail(Email email) {
        LOGGER.info("Entering put with email " + securityContext.getUserPrincipal().toString());
        try {
            if (email == null) {
                throw new IllegalArgumentException(
                        "Can't edit department since it does not exist in the database");
            }
            emailRepo.updateEmail(email);
            return Response.ok().build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
        }
    }
}
