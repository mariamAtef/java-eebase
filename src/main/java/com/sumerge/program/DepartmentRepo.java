package com.sumerge.program;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class DepartmentRepo {
    @PersistenceContext(unitName = "MyPU")
    private EntityManager em;


    public List<Department> getAllDepartment() {
        try {
            String sql = "SELECT d FROM Department d";
            List<Department> departments = em.createQuery(sql, Department.class).getResultList();
            return departments;
        } catch (JpaException ex) {
            throw new JpaException(ex.getMessage(), ex);
        }
    }

    public Department getCertainDepartment(String id) {
        try {
            Department departments = em.find(Department.class, id);
            return departments;
        } catch (JpaException ex) {
            throw new JpaException(ex.getMessage(), ex);
        }
    }

    public void addDepartment(Department department) {
        try {
            em.getTransaction().begin();
            em.persist(department);
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }

    public void updateDepartment(Department department) {
        try {
            em.getTransaction().begin();
            Department department1 = getCertainDepartment(department.getDeptCode());
            department1.setDeptName(department1.getDeptName());
            department1.setManager(department1.getManager());
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }

    public void deleteDepartment(String id) {
        try {
            em.getTransaction().begin();
            Department department = getCertainDepartment(id);
            em.remove(department);
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }


}
