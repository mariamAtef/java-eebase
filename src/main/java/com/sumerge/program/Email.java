package com.sumerge.program;

import javax.persistence.*;

@Entity
@Table(name = "EMAIL")
public class Email {
    @Id
    @Column(name= "EMAILID")
    private int emailId;


    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee empId;

    @Column(name = "EMAILADDRESS")
    private String email;

    @Column(name = "EMAILTYPE")
    private String emailType;

    public int getEmailId() {
        return emailId;
    }

    public Employee getEmpId() {
        return empId;
    }

    public String getEmail() {
        return email;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmpId(int empId) {
        empId = empId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }
}
