package com.sumerge.program;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


/** Configuration of the main server path.
 * @author Mawaziny.
 */
@ApplicationPath("user")
public class JAXRSConfiguration extends Application {

}
