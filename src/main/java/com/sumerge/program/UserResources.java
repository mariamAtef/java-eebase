package com.sumerge.program;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.net.URI;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import javax.ws.rs.core.SecurityContext;



@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("users")

public class UserResources {
  private static final Logger LOGGER = Logger.getLogger(UserResources.class.getName());

  private UserController userController = new UserController();

  @Context HttpServletRequest request;

  @Context
  private SecurityContext securityContext;


    /**
   *  Making a Get HTTP request to get all users.
   * @return
   */
  @GET
  public Response getAllUsers() {
      LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
    try {
      return Response.ok().entity(userController.getAllUsers()).build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Making a Get HTTP request to get a certain user by his id.
   * @param id id of the user
   * @return
   */
  @GET
  @Path("{id}")
  public Response getUserById(@PathParam("id") int id) {
      LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
    try {
      return Response.ok().entity(userController.getUserById(id)).build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Making a Get HTTP request to get a certain user by his email.
   * @param email email of the user
   * @return
   */
  @GET
  public Response getUserByEmail(@QueryParam("email") String email) {
      LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
    try {
      return Response.ok().entity(userController.getUserByEmail(email)).build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Making a Get HTTP request to get a certain user by his address.
   * @param address address of the user.
   * @return
   */
  @GET
  public Response getUserByAddress(@QueryParam("address") String address) {
      LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
    try {
      return Response.ok().entity(userController.getUserByAddress(address)).build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Making a Get HTTP request to get a certain user by his name.
   * @param name name of the user
   * @return
   */
  @GET
  @Path("{name}")
  public Response getUserByName(@QueryParam("name") String name) {
    try {
        LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
      return Response.ok().entity(userController.getUserByName(name)).build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Making a Delete HTTP reuest to delete a certain user by his id.
   * @param id of the user
   * @return
   */

  @DELETE
  @Path("{id}")
  public Response deleteStudent(@PathParam("id") int id) {
      LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
    try {
      userController.deleteUser(id);
      return Response.ok().build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Making a Post HTTP request to add a new user.
   * @param user All user params
   * @return
   */
  @POST
  public Response addStudent(User user) {
      LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
    try {
      if (user == null) {
        throw new IllegalArgumentException("Can't create student since it exists in the database");
      }
      userController.addUser(
          user.getName(),
          user.getId(),
          user.getEmail(),
          user.getAddresss(),
          user.getMobileNumber());
      URI uri = new URI(request.getRequestURI());
      return Response.created(uri).build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }

  /**
   * Making a Put HTTP request to update a user's information.
   * @param user All user params
   * @return
   */
  @PUT
  public Response editStudent(User user) {
      LOGGER.info("Entering get with user " + securityContext.getUserPrincipal().toString());
    try {
      if (user == null) {
        throw new IllegalArgumentException(
            "Can't edit student since it does not exist in the database");
      }
      userController.updateUserInfo(user);
      return Response.ok().build();
    } catch (Exception e) {
      LOGGER.log(SEVERE, e.getMessage(), e);
      return Response.serverError().entity(e.getClass() + ": " + e.getMessage()).build();
    }
  }
}
