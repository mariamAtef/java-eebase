package com.sumerge.program;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class EmailRepo {
    @PersistenceContext(unitName = "MyPU")
    private EntityManager em;


    public List<Email> getAllEmails() {
        try {
            String sql = "SELECT e FROM Email e";
            List<Email> emailDetails = em.createQuery(sql, Email.class).getResultList();
            return emailDetails;
        } catch (JpaException ex) {
            throw new JpaException(ex.getMessage(), ex);
        }
    }

    public Email getCertainEmail(int id) {
        try {
            Email emailDetails = em.find(Email.class, id);
            return emailDetails;
        } catch (JpaException ex) {
            throw new JpaException(ex.getMessage(), ex);
        }
    }

    public void addEmmail(Email email) {
        try {
            em.getTransaction().begin();
            em.persist(email);
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }

    public void updateEmail(Email email) {
        try {
            em.getTransaction().begin();
            Email details = getCertainEmail(email.getEmailId());
            details.setEmail(email.getEmail());
            details.setEmailType(email.getEmailType());
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }

    public void deleteEmail(int id) {
        try {
            em.getTransaction().begin();
            Email email = getCertainEmail(id);
            em.remove(email);
            em.getTransaction().commit();
        } catch (JpaException e) {
            em.getTransaction().rollback();
            throw new JpaException(e.getMessage(), e);
        }
    }
}
