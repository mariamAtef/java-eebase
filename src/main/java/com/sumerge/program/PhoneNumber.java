package com.sumerge.program;

import javax.persistence.*;

@Entity
@Table(name = "PHONENUMBER")
public class PhoneNumber {
    @Id
    @Column(name = "PHONEID")
    private String phoneId;


    @ManyToOne
    @JoinColumn(name = "EMPID")
    private Employee empId;

    @Column(name = "LOCALNUM")
    private String localNum;

    @Column(name = "INTLPREFIX")
    private String intlPrefix;

    @Column(name = "PHONETYPE")
    private String phoneType;


    public String getPhoneId() {
        return phoneId;
    }

    public Employee getEmpId() {
        return empId;
    }

    public String getLocalNum() {
        return localNum;
    }

    public String getIntlPrefix() {
        return intlPrefix;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setLocalNum(String localNum) {
        this.localNum = localNum;
    }

    public void setIntlPrefix(String intlPrefix) {
        this.intlPrefix = intlPrefix;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }
}
