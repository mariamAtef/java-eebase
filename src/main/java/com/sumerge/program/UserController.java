package com.sumerge.program;

import java.util.ArrayList;

public class UserController {
  public static ArrayList<User> users =
      new ArrayList<User>() {
        {
          add(new User("mariam", 123453, "mariamatef@outlook.com", "Nozha", 7448));
        }
      };

  /**
   * Adding a new user.
   *
   * @param name name of the user.
   * @param id id of the user.
   * @param email email of the user.
   * @param addresss address of the user.
   * @param mobileNumber mobile number of the user.
   */
  public void addUser(String name, int id, String email, String addresss, int mobileNumber) {
    User user = new User(name, id, email, addresss, mobileNumber);
    users.add(user);
  }

  public ArrayList<User> getAllUsers() {
    return users;
  }

  /**
   * getting a user's information by name.
   *
   * @param name name of the user.
   * @return the details of user that has the input name.
   */
  public User getUserByName(String name) {
    int index = 0;
    for (int i = 0; i < users.size(); i++) {
      if (users.get(i).getName().equals(name)) {
        index = i;
        break;
      }
    }
    return users.get(index);
  }

  /**
   * getting a user's information by id.
   *
   * @param id id of the user
   * @return the details of user that has the input id.
   */
  public User getUserById(int id) {
    int index = 0;
    for (int i = 0; i < users.size(); i++) {
      if (users.get(i).getId() == id) {
        index = i;
        break;
      }
    }
    return users.get(index);
  }

  /**
   * getting a user's information by email.
   *
   * @param email email of the user.
   * @return the details of user that has the input id.
   */
  public User getUserByEmail(String email) {
    int index = 0;
    for (int i = 0; i < users.size(); i++) {
      if (users.get(i).getEmail().equals(email)) {
        index = i;
        break;
      }
    }
    return users.get(index);
  }

  /**
   * getting a user's information by address.
   *
   * @param address address of the user.
   * @return the details of user that has the input address.
   */
  public User getUserByAddress(String address) {
    int index = 0;
    for (int i = 0; i < users.size(); i++) {
      if (users.get(i).getAddresss().equals(address)) {
        index = i;
        break;
      }
    }
    return users.get(index);
  }

  /**
   * delete a certain user using his id.
   *
   * @param id user id
   */
  public void deleteUser(int id) {
    for (int i = 0; i < users.size(); i++) {
      if (users.get(i).getId() == id) {
        users.remove(i);
      }
    }
  }

  /**
   * Updating a certain user parameters.
   * @param user User that i want to update params
   */
  public void updateUserInfo(User user) {
    for (int i = 0; i < users.size(); i++) {
      if (users.get(i).getId() == user.getId()) {
        users.get(i).setEmail(user.getEmail());
        users.get(i).setAddresss(user.getAddresss());
        users.get(i).setMobileNumber(user.getMobileNumber());
      }
    }
  }
}
