package com.sumerge.program;

import javax.persistence.*;

@Entity
@Table(name = "PHOTO")
public class Photo {
    @Id
    @Column(name = "PHOTOID")
    private int photoId;

    @JoinColumn(name = "EMPID")
    @ManyToOne
    private Employee empId;

    @Column(name = "IMAGENAME")
    private String imageName;

    public int getPhotoId() {
        return photoId;
    }

    public Employee getEmpId() {
        return empId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
